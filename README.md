# re-partners

## Getting started

The main repo to work with all other modules. Utilizes `git subtree`.

Currently, we are utilizing the following repos:

* [api](https://gitlab.com/talgat.s/re-partners-api) - `api` directory
* [infrastructure](https://gitlab.com/talgat.s/re-partners-infra) - `infra` directory

## Run with docker compose

Only for dev purpose. Will probably move to [k8](https://kubernetes.io/).

1. Install [docker for desktop](https://www.docker.com/products/docker-desktop/).
2. Run docker compose

```bash
docker compose -f ./infra/docker-compose.yml --env-file=./.env up --build
```

### How to connect

* api: Port: please check env variable `API_EXTERNAL_PORT`

## Migrations

### Create

```shell
docker compose -f ./infra/docker-compose.yml --env-file=./.env --profile tools run --rm migrate create -ext sql -dir /migrations <MIGRATION_NAME>

docker compose -f ./infra/docker-compose.yml --env-file=./.env --profile tools run --rm migrate create -ext sql -dir /migrations create_pack_table
```

### Up

```shell
docker compose -f ./infra/docker-compose.yml --env-file=./.env --profile tools run --rm migrate up
```

### Down

```shell
docker compose -f ./infra/docker-compose.yml --env-file=./.env --profile tools run --rm migrate down
```

## Subtree (Please don't do anything if you are not familiar, ask Talgat)

### Add

First add remote for a subtree to shorten the pull.

```shell
git remote add <REMOTE_NAME> <SUBTREE_URL>

git remote add api git@gitlab.com:talgat.s/re-partners-go.git
```

Add subtree.

```shell
git subtree add --prefix <SUBTREE_NAME> <REMOTE_NAME> main --squash

git subtree add --prefix api api main --squash
```

### Update

```shell
git fetch <REMOTE_NAME> main
git subtree pull --prefix <SUBTREE_NAME> <REMOTE_NAME> main --squash

git fetch api main
git subtree pull --prefix api api main --squash
```


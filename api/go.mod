module gitlab.com/talgat.s/re-partners-api

go 1.21

require (
	github.com/joho/godotenv v1.5.1
	github.com/sethvargo/go-envconfig v0.9.0
)

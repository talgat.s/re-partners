package handler

import (
	"encoding/json"
	"fmt"
	"net/http"
)

// Pack handler for all methods
func (h *handlerObject) Pack(w http.ResponseWriter, r *http.Request) {
	h.api.GetLog().InfoContext(r.Context(), "start Pack")

	switch r.Method {
	case http.MethodGet:
		h.getAllPacks(w, r)
	case http.MethodPut:
		h.replaceAllPacks(w, r)
	case http.MethodOptions:
		w.Header().Set("Allow", "GET, PUT, OPTIONS")
		w.WriteHeader(http.StatusNoContent)
	default:
		mes := fmt.Sprintf("method: %s not allowed", r.Method)
		h.api.GetLog().InfoContext(r.Context(), "error Pack", "error", mes)
		w.Header().Set("Allow", "GET, PUT, OPTIONS")
		http.Error(w, "method not allowed", http.StatusMethodNotAllowed)
	}
}

// getAllPacks get all packs sizes
func (h *handlerObject) getAllPacks(w http.ResponseWriter, r *http.Request) {
	// id := c.Params("id")
	// db := database.DB
	// var user model.User
	// db.Find(&user, id)
	// if user.Username == "" {
	// 	return c.Status(404).JSON(fiber.Map{"status": "error", "message": "No user found with ID", "data": nil})
	// }
	// return c.JSON(fiber.Map{"status": "success", "message": "User found", "data": user})

	h.api.GetLog().InfoContext(r.Context(), "start getAllPacks", "method", "GET")

	sizes := []int{250, 500, 1000, 2000, 5000}

	w.WriteHeader(http.StatusOK)
	w.Header().Set("Content-Type", "application/json")
	resp := map[string]interface{}{
		"status":  "success",
		"message": "pong",
		"data": map[string]interface{}{
			"sizes": sizes,
		},
	}
	if err := json.NewEncoder(w).Encode(resp); err != nil {
		mes := fmt.Sprintf("error building the response, %v", err)
		h.api.GetLog().InfoContext(r.Context(), "error getAllPacks", "error", mes)
		http.Error(w, mes, http.StatusInternalServerError)
		return
	}
}

// replaceAllPacks get all packs sizes
func (h *handlerObject) replaceAllPacks(w http.ResponseWriter, r *http.Request) {
	h.api.GetLog().InfoContext(r.Context(), "start replaceAllPacks", "method", "GET")

	w.WriteHeader(http.StatusOK)
	w.Header().Set("Content-Type", "application/json")
	resp := map[string]interface{}{}
	if err := json.NewEncoder(w).Encode(resp); err != nil {
		mes := fmt.Sprintf("error building the response, %v", err)
		h.api.GetLog().InfoContext(r.Context(), "error replaceAllPacks", "error", mes)
		http.Error(w, mes, http.StatusInternalServerError)
		return
	}
}

// // ReplaceAllPacks replace packs sizes
// func (h *handlerObject) ReplaceAllPacks(c *fiber.Ctx) error {
// 	type NewUser struct {
// 		Username string `json:"username"`
// 		Email    string `json:"email"`
// 	}
//
// 	db := database.DB
// 	user := new(model.User)
// 	if err := c.BodyParser(user); err != nil {
// 		return c.Status(500).JSON(fiber.Map{"status": "error", "message": "Review your input", "errors": err.Error()})
// 	}
//
// 	validate := validator.New()
// 	if err := validate.Struct(user); err != nil {
// 		return c.Status(fiber.StatusBadRequest).JSON(fiber.Map{"message": "Invalid request body", "errors": err.Error()})
// 	}
//
// 	hash, err := hashPassword(user.Password)
// 	if err != nil {
// 		return c.Status(500).JSON(fiber.Map{"status": "error", "message": "Couldn't hash password", "errors": err.Error()})
// 	}
//
// 	user.Password = hash
// 	if err := db.Create(&user).Error; err != nil {
// 		return c.Status(500).JSON(fiber.Map{"status": "error", "message": "Couldn't create user", "errors": err.Error()})
// 	}
//
// 	newUser := NewUser{
// 		Email:    user.Email,
// 		Username: user.Username,
// 	}
//
// 	return c.JSON(fiber.Map{"status": "success", "message": "Created user", "data": newUser})
// }

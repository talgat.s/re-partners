package handler

import (
	"net/http"

	"gitlab.com/talgat.s/re-partners-api/cmd/api"
	"gitlab.com/talgat.s/re-partners-api/cmd/database"
)

type Handler interface {
	Ping(w http.ResponseWriter, r *http.Request)
	Pack(w http.ResponseWriter, r *http.Request)
	OrderItem(w http.ResponseWriter, r *http.Request)
}

type handlerObject struct {
	api api.Api
	db  database.DB
}

func New(api api.Api, db database.DB) Handler {
	return &handlerObject{
		api: api,
		db:  db,
	}
}

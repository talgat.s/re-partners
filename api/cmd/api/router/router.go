package router

import (
	"net/http"

	"gitlab.com/talgat.s/re-partners-api/cmd/api"
	"gitlab.com/talgat.s/re-partners-api/cmd/api/handler"
	"gitlab.com/talgat.s/re-partners-api/cmd/database"
)

// SetupRoutes setup router api
func SetupRoutes(mux *http.ServeMux, api api.Api, db database.DB) {
	h := handler.New(api, db)

	mux.HandleFunc("/api/ping", h.Ping)
	mux.HandleFunc("/api/pack", h.Pack)
	mux.HandleFunc("/api/order", h.OrderItem)

	// // Group
	// apiGroup := app.Group("/api")
	//
	// apiGroup.Get("/ping", h.Pong)
	//
	// // Pack
	// pack := apiGroup.Group("/pack")
	// pack.Get("/", h.GetAllPacks)
	// pack.Put("/", h.ReplaceAllPacks)

	// Order
	// order := api.Group("/order")
	// order.Get("/:numItems", h.GetOrder)
}

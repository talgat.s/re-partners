package api

import (
	"context"
	"log/slog"

	"gitlab.com/talgat.s/re-partners-api/cmd/database"
)

type Api interface {
	Start(ctx context.Context, cancel context.CancelFunc, db database.DB)
	GetLog() *slog.Logger
}

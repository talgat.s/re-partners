package database

import (
	"log/slog"

	"gitlab.com/talgat.s/re-partners-api/configs"
)

type DB interface {
}

type dbService struct {
	log  *slog.Logger
	conf *configs.DatabaseConfig
}

func New(log *slog.Logger, conf *configs.DatabaseConfig) DB {
	db := &dbService{
		log:  log,
		conf: conf,
	}

	return db
}

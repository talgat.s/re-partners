package configs

import (
	"context"
	"flag"

	"github.com/sethvargo/go-envconfig"
)

type DatabaseConfig struct {
	*SharedConfig `env:",prefix=DATABASE_"`
}

func newDatabaseConfig(ctx context.Context) (*DatabaseConfig, error) {
	c := &DatabaseConfig{}

	if err := envconfig.Process(ctx, c); err != nil {
		return nil, err
	}

	flag.IntVar(&c.Port, "database-port", c.Port, "database port [DATABASE_PORT]")
	flag.StringVar(&c.Host, "database-host", c.Host, "database host [DATABASE_HOST]")

	return c, nil
}

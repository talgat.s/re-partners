package configs

import (
	"context"
	"flag"

	"gitlab.com/talgat.s/re-partners-api/internal/constant"
)

type SharedConfig struct {
	Port int    `env:"PORT"`
	Host string `env:"HOST,default=localhost"`
}

type Config struct {
	Env      constant.Environment
	Api      *ApiConfig
	Database *DatabaseConfig
}

func NewConfig(ctx context.Context) (*Config, error) {
	conf := &Config{
		Env: getEnv(),
	}

	_ = conf.loadDotEnvFiles()

	// Api config
	if c, err := newApiConfig(ctx, conf.Env); err != nil {
		return nil, err
	} else {
		conf.Api = c
	}

	// Database config
	if c, err := newDatabaseConfig(ctx); err != nil {
		return nil, err
	} else {
		conf.Database = c
	}

	flag.Parse()

	return conf, nil
}

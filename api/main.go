package main

import (
	"context"

	"gitlab.com/talgat.s/re-partners-api/cmd/api/server"
	"gitlab.com/talgat.s/re-partners-api/cmd/database"
	"gitlab.com/talgat.s/re-partners-api/configs"
	"gitlab.com/talgat.s/re-partners-api/internal/constant"
	"gitlab.com/talgat.s/re-partners-api/pkg/logger"
)

func main() {
	ctx, cancel := context.WithCancel(context.Background())

	// conf
	conf, err := configs.NewConfig(ctx)
	if err != nil {
		panic(err)
	}

	// setup logger
	log := logger.New(conf)

	// configure database service
	db := database.New(log.With("service", constant.Database), conf.Database)

	// configure api service
	srv := server.New(log.With("service", constant.Api), conf.Api)
	// start api service
	srv.Start(ctx, cancel, db)

	<-ctx.Done()
	// Your cleanup tasks go here
	log.InfoContext(ctx, "cleaning up ...")

	log.InfoContext(ctx, "server was successfully shutdown.")
}

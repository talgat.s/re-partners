package logger

import (
	"log/slog"
	"os"

	"gitlab.com/talgat.s/re-partners-api/configs"
	"gitlab.com/talgat.s/re-partners-api/internal/constant"
)

func New(conf *configs.Config) *slog.Logger {
	if conf.Env != constant.EnvironmentLocal {
		return slog.New(slog.NewJSONHandler(os.Stdout, nil))
	}

	return slog.New(slog.NewTextHandler(os.Stdout, nil))
}
